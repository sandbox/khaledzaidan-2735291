<?php
/**
 * @file
 * This file implements the admin UI for Shortcode Custom.
 */

/**
 * The form callback for the administration form.
 */
function shortcode_custom_administer_form() {
  $form = array();

  $form['shortcodes'] = array();

  $shortcode_item['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Shortcode Label'),
  );

  $form['shortcodes'][] = $shortcode_item;

  return $form;
}